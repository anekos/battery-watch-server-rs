
extern crate iron;
#[macro_use]
extern crate router;
#[macro_use]
extern crate lazy_static;
extern crate argparse;
extern crate r2d2;
extern crate r2d2_sqlite;
extern crate chrono;


use std::ops::Sub;
use std::sync::Arc;
use argparse::{ArgumentParser, Store};
use r2d2_sqlite::SqliteConnectionManager;
use iron::prelude::*;
use iron::status;
use router::*;
use std::net::{SocketAddrV4, Ipv4Addr};
use std::fmt;
use chrono::*;
use std::thread::spawn;


lazy_static! {
    static ref POOL: Arc<r2d2::Pool<SqliteConnectionManager>> = {
        let manager = SqliteConnectionManager::new("levels.db");
        let config = r2d2::Config::builder().pool_size(16).build();
        return Arc::new(r2d2::Pool::new(config, manager).unwrap());
    };
}

struct Level {
    device: String,
    level: i32,
    at: String
}

struct Levels {
    user: String,
    entries: Vec<Level>
}

impl fmt::Display for Level {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let local = Local::now();

        let diff = {
            let at = format!("{} +00:00", self.at);
            let at = DateTime::parse_from_str(&*at, "%Y-%m-%d %H:%M:%S %z").unwrap();
            let d = local.sub(at);
            if d.num_days() > 1 {
                format!("{}d", d.num_days())
            } else if d.num_hours() > 1 {
                format!("{}h", d.num_hours())
            } else {
                format!("{}m", d.num_minutes())
            }
        };

        write!(f, "{}\t{}\t{}", self.device, self.level, diff)
    }
}

impl fmt::Display for Levels {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut buf = String::new();

        buf.push_str(&self.user);
        buf.push_str("\n");

        for entry in &self.entries {
            buf.push_str(&entry.to_string());
            buf.push_str("\n");
        }

        write!(f, "{}", buf)
    }
}


fn initialize_db() -> () {
    let conn = &(*POOL.get().unwrap());

    let mut stmt = conn.prepare("SELECT name FROM sqlite_master WHERE type='table' AND name='levels'").unwrap();
    let mut rows = stmt.query(&[]).unwrap();
    if !rows.next().is_some() {
        conn.execute("
        CREATE TABLE levels (
          user varchar(80),
          device varchar(80),
          level integer,
          created_at varchar(19) default (datetime())
        );",
        &[]).unwrap();
    }
}


fn update_level(user: &str, device: &str, level: i32) -> () {
    let conn = &(*POOL.get().unwrap());

    conn.execute(
        "INSERT INTO levels (user, device, level) VALUES ($1, $2, $3)",
        &[&user, &device, &level]).unwrap();
}

fn delete_old() {
    let conn = &(*POOL.get().unwrap());

    conn.execute("DELETE FROM levels WHERE created_at < datetime('now', '-7 day')", &[]).unwrap();
}

fn get_levels(user: &str) -> Levels {
    let select = "SELECT device, level, created_at FROM levels AS m WHERE NOT EXISTS(SELECT 1 FROM levels AS s WHERE m.device = s.device AND m.created_at < s.created_at) AND m.user = $1";

    let conn = &(*POOL.get().unwrap());
    let mut stmt = conn.prepare(select).unwrap();

    let iter = stmt.query_map(&[&user], |row| {
        Level {
            device: row.get(0),
            level: row.get(1),
            at: row.get(2)
        }
    }).unwrap();

    let es = iter.map(|it| it.unwrap()).collect();

    Levels {
        user: user.to_string(),
        entries: es
    }
}


fn handler_index(request: &mut Request) -> Result<Response, IronError> {
    println!("request => {}", request.headers);
    Ok(Response::with((status::Ok, "Metallica")))
}

fn handler_levels(request: &mut Request) -> Result<Response, IronError> {
    let user = request.extensions.get::<Router>().unwrap().find("user").unwrap();

    let levels = get_levels(user);

    spawn(move || delete_old());

    println!("levels => {}", levels);

    Ok(Response::with((status::Ok, levels.to_string())))
}

fn handler_update(request: &mut Request) -> Result<Response, IronError> {
    let r = request.extensions.get::<Router>().unwrap();
    let user = r.find("user").unwrap();
    let device = r.find("device").unwrap();
    let level = r.find("level").unwrap().parse::<i32>().unwrap();

    println!("user = {}, device = {}, level = {}", user, device, level);

    update_level(user, device, level);

    Ok(Response::with((status::Ok, "OK")))
}


fn main() {
    let mut port = 3000;

    {
        let mut ap = ArgumentParser::new();
        ap.refer(&mut port).add_option(&["-p", "--port"], Store, "Port number");
        ap.parse_args_or_exit();
    }

    initialize_db();

    {
        let host = Ipv4Addr::new(127, 0, 0, 1);
        let router = router!(
            index: get "/" => handler_index,
            levels: get "/:user" => handler_levels,
            update: get "/:user/:device/:level" => handler_update);
        match Iron::new(router).http(SocketAddrV4::new(host, port)) {
            Ok(_) => println!("Launched"),
            Err(err) => println!("{}", err)
        }
    }
}
